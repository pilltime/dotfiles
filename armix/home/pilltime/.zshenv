export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_DATA_DIRS='/usr/local/share:/usr/share'
export XDG_CONFIG_DIRS='/etc/xdg'

export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$ZDOTDIR/.histfile"
export HISTSIZE='10000'
export SAVEHIST='10000'

export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"

export GLFW_IM_MODULE='ibus'
export SDL_IM_MODULE='fcitx'
export GTK_IM_MODULE='fcitx'
export QT_IM_MODULE='fcitx'
export XMODIFIERS='@im=fcitx'

export EDITOR='nvim'
export VISUAL='nvim'

export LESSHISTFILE='-'

export BAT_THEME='gruvbox-dark'
export PAGER='bat'
export MANPAGER='sh -c "col -bx | bat -l man -p"'
export MANROFFOPT='-c'

export PATH="$PATH:$HOME/.local/bin:$CARGO_HOME/bin"
